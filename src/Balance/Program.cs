﻿using Hashgraph;
using System;
using System.Threading.Tasks;

namespace Balance
{
    static class Program
    {
        private static async Task Main(string[] args)
        {
            const long accountNumber = 2;
            Console.WriteLine("HBAR balance");
            var address = new Address(0, 0, accountNumber);
            var gateway = new Gateway("35.237.200.180:50211", 0, 0, 3);
            await using var client = new Client(ctx => { ctx.Gateway = gateway; });
            var balanceTinyBars = (decimal)await client.GetAccountBalanceAsync(address);
            var balanceHbar = balanceTinyBars / 100_000_000;
            Console.WriteLine($"Account Balance for {accountNumber} is {balanceHbar} HBAR"); 
        }
    }
}